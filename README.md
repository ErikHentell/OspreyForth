# OspreyForth

OspreyForth is an attempt to connect the pForth interpreter to the Mac OS X and GNUStep Objective-C Runtime

**Background**
==============
OspreyForth began life by using the FICL Forth interpreter, but the pForth system seemed simpler to work with. In addition, pForth can save the code out as an intermediate binary file that can be incorporated into a new pForth binary; this could be useful for creating standalone software, rather than relying on the pForth interpreter to run a script. Unfortunately, due to work and other issues, I've had to start and stop this project several times. 

The purpose of this project is to create a Forth system that can replace shell scripts and be used for general utilities and other work. I am aware that there are Forth systems out there, including systems capable of using the Mac OS X Objective-C Runtime API. That said, I wanted to create a Forth system all my own, and one that I had a relatively strong understanding of with regards to the internals.

**Current Status**
------------------
Currently, the initial build of the system is almost complete. Almost all of the Objective-C Runtime API has been connected via C code, but the system has not been compiled. Next steps will include compiling, fixing errors, compiling some more, and so on. After this, OspreyForth will be used to handle shell scripting, creation of utilities, and other tasks until a variation can be developed that will work with GNUStep. From there, perhaps more ambitious projects can be attempted.

**TODO**
--------
* Engage the compile-fix-compile cycle with the code
* Once the code compiles successfully, start testing the system with general utility scripts
* Start adapting the code for GNUStep
* Clean up and comment code for anyone who wants to take a look at it.
