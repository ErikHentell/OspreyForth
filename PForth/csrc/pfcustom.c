/* @(#) pfcustom.c 98/01/26 1.3 */

#ifndef PF_USER_CUSTOM

/***************************************************************
** Call Custom Functions for pForth
**
** Create a file similar to this and compile it into pForth
** by setting -DPF_USER_CUSTOM="mycustom.c"
**
** Using this, you could, for example, call X11 from Forth.
** See "pf_cglue.c" for more information.
**
** Author: Phil Burk
** Copyright 1994 3DO, Phil Burk, Larry Polansky, David Rosenboom
**
** Permission to use, copy, modify, and/or distribute this
** software for any purpose with or without fee is hereby granted.
**
** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
** WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
** THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
** CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
** FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
** CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
**
***************************************************************/


#include "pf_all.h"
#include "objc/class.h"
#include "objc/instance.h"
#include "objc/ivar.h"
#include "objc/library.h"
#include "objc/message.h"
#include "objc/method.h"
#include "objc/nsbun.h"
#include "objc/nsstr.h"
#include "objc/property.h"
#include "objc/protocol.h"
#include "objc/sel.h"

static cell_t CTest0( cell_t Val );
static void CTest1( cell_t Val1, cell_t Val2 );

/****************************************************************
** Step 1: Put your own special glue routines here
**     or link them in from another file or library.
****************************************************************/
static cell_t CTest0( cell_t Val )
{
    MSG_NUM_D("CTest0: Val = ", Val);
    return Val+1;
}

static void CTest1( cell_t Val1, cell_t Val2 )
{

    MSG("CTest1: Val1 = "); ffDot(Val1);
    MSG_NUM_D(", Val2 = ", Val2);
}

/****************************************************************
** Step 2: Create CustomFunctionTable.
**     Do not change the name of CustomFunctionTable!
**     It is used by the pForth kernel.
****************************************************************/

#ifdef PF_NO_GLOBAL_INIT
/******************
** If your loader does not support global initialization, then you
** must define PF_NO_GLOBAL_INIT and provide a function to fill
** the table. Some embedded system loaders require this!
** Do not change the name of LoadCustomFunctionTable()!
** It is called by the pForth kernel.
*/
#define NUM_CUSTOM_FUNCTIONS  (2)
CFunc0 CustomFunctionTable[NUM_CUSTOM_FUNCTIONS];

Err LoadCustomFunctionTable( void )
{
    CustomFunctionTable[0] = CTest0;
    CustomFunctionTable[1] = CTest1;
    return 0;
}

#else
/******************
** If your loader supports global initialization (most do.) then just
** create the table like this.
*/
CFunc0 CustomFunctionTable[] =
{
    (CFunc0) CTest0,
    (CFunc0) CTest1,
    (CFunc0) objc_class_getname,
    (CFunc0) objc_class_getsuperclass,
    (CFunc0) objc_class_ismetaclass,
    (CFunc0) objc_class_getinstancesize,
    (CFunc0) objc_class_getinstancevariable,

    (CFunc0) objc_class_getclassvariable,
    (CFunc0) objc_class_addivar,
    (CFunc0) objc_class_copyivarlist,
    (CFunc0) objc_class_getivarlayout,
    (CFunc0) objc_class_setivarlayout,

    (CFunc0) objc_class_getweakivarlayout,
    (CFunc0) objc_class_setweakivarlayout,
    (CFunc0) objc_class_getproperty,
    (CFunc0) objc_class_copypropertylist,
    (CFunc0) objc_class_addmethod,

    (CFunc0) objc_class_getinstancemethod,
    (CFunc0) objc_class_getclassmethod,
    (CFunc0) objc_class_copymethodlist,
    (CFunc0) objc_class_replacemethod,
    (CFunc0) objc_class_getmethodimplementation,

    (CFunc0) objc_class_getmethodimplementationstret,
    (CFunc0) objc_class_respondstoselector,
    (CFunc0) objc_class_addprotocol,
    (CFunc0) objc_class_addproperty,
    (CFunc0) objc_class_replaceproperty,

    (CFunc0) objc_class_conformstoprotocol,
    (CFunc0) objc_class_copyprotocollist,
    (CFunc0) objc_class_getversion,
    (CFunc0) objc_class_setversion,

    (CFunc0) objc_class_allocateclasspair,
    (CFunc0) objc_class_disposeclasspair,
    (CFunc0) objc_class_registerclasspair,

    (CFunc0) objc_class_createinstance,
    (CFunc0) objc_class_constructinstance,
    (CFunc0) objc_class_destructinstance,

    (CFunc0) objc_class_getclasslist,
    (CFunc0) objc_class_freeclasslist,
    (CFunc0) objc_class_lookupclass,
    (CFunc0) objc_class_getclass,
    (CFunc0) objc_class_getrequiredclass,
    (CFunc0) objc_class_getmetaclass,

    (CFunc0) objc_ivar_getname,
    (CFunc0) objc_ivar_gettypeencoding,

    (CFunc0) objc_lib_getname,
    (CFunc0) objc_lib_getimagename,
    (CFunc0) objc_lib_copyclassnamesforimage,

    (CFunc0) objc_sel_getname,
    (CFunc0) objc_sel_registername,
    (CFunc0) objc_sel_getuid,
    (CFunc0) objc_sel_isequal,

    (CFunc0) objc_property_getname,
    (CFunc0) objc_property_getattributes,

    (CFunc0) objc_protocol_getprotocol,
    (CFunc0) objc_protocol_copyprotocollist,
    (CFunc0) objc_protocol_getname,
    (CFunc0) objc_protocol_isequal,
    (CFunc0) objc_protocol_copymethoddescriptionlist,
    (CFunc0) objc_protocol_conformstoprotocol,
    (CFunc0) objc_protocol_getproperty,
    (CFunc0) objc_protocol_copymethoddescription,
    (CFunc0) objc_protocol_copypropertylist,
    (CFunc0) objc_protocol_copyprotocollist2,

    (CFunc0) objc_object_copy,
    (CFunc0) objc_object_dispose,
    (CFunc0) objc_object_setinstvar,
    (CFunc0) objc_object_getinstvar,
    (CFunc0) objc_object_getindexedivars,
    (CFunc0) objc_object_getivar,
    (CFunc0) objc_object_setivar,
    (CFunc0) objc_object_getclassname,
    (CFunc0) objc_object_getclass,
    (CFunc0) objc_object_setclass,
    
    (CFunc0) objc_method_getname,
    (CFunc0) objc_method_getimplementation,
    (CFunc0) objc_method_gettypeencoding,
    (CFunc0) objc_method_copyreturntype,
    (CFunc0) objc_method_copyargumenttype,
    (CFunc0) objc_method_getreturntype,
    (CFunc0) objc_method_getnumberofarguments,
    (CFunc0) objc_method_getargumenttype,
    (CFunc0) objc_method_copydescription,
    (CFunc0) objc_method_setimplementation,
    (CFunc0) objc_method_exchangeimplementations,
    (CFunc0) objc_method_invoke00,
    (CFunc0) objc_method_invoke01,
    (CFunc0) objc_method_invoke02,
    (CFunc0) objc_method_invoke03,
    (CFunc0) objc_method_invoke04,
    (CFunc0) objc_method_invoke05,
    (CFunc0) objc_method_invoke06,
    (CFunc0) objc_method_invoke07,
    (CFunc0) objc_method_invoke08,
    (CFunc0) objc_method_invoke09,
    (CFunc0) objc_method_invoke10,
    (CFunc0) objc_method_invokestret00,
    (CFunc0) objc_method_invokestret01,
    (CFunc0) objc_method_invokestret02,
    (CFunc0) objc_method_invokestret03,
    (CFunc0) objc_method_invokestret04,
    (CFunc0) objc_method_invokestret05,
    (CFunc0) objc_method_invokestret06,
    (CFunc0) objc_method_invokestret07,
    (CFunc0) objc_method_invokestret08,
    (CFunc0) objc_method_invokestret09,
    (CFunc0) objc_method_invokestret10,
    
    (CFunc0) objc_message_send00,
    (CFunc0) objc_message_send01,
    (CFunc0) objc_message_send02,
    (CFunc0) objc_message_send03,
    (CFunc0) objc_message_send04,
    (CFunc0) objc_message_send05,
    (CFunc0) objc_message_send06,
    (CFunc0) objc_message_send07,
    (CFunc0) objc_message_send08,
    (CFunc0) objc_message_send09,
    (CFunc0) objc_message_send10,
    
    (CFunc0) objc_message_send_super00,
    (CFunc0) objc_message_send_super01,
    (CFunc0) objc_message_send_super02,
    (CFunc0) objc_message_send_super03,
    (CFunc0) objc_message_send_super04,
    (CFunc0) objc_message_send_super05,
    (CFunc0) objc_message_send_super06,
    (CFunc0) objc_message_send_super07,
    (CFunc0) objc_message_send_super08,
    (CFunc0) objc_message_send_super09,
    (CFunc0) objc_message_send_super10,
    
    (CFunc0) objc_message_sendfpret00,
    (CFunc0) objc_message_sendfpret01,
    (CFunc0) objc_message_sendfpret02,
    (CFunc0) objc_message_sendfpret03,
    (CFunc0) objc_message_sendfpret04,
    (CFunc0) objc_message_sendfpret05,
    (CFunc0) objc_message_sendfpret06,
    (CFunc0) objc_message_sendfpret09,
    (CFunc0) objc_message_sendfpret10,
    
    (CFunc0) objc_message_sendstret00,
    (CFunc0) objc_message_sendstret01,
    (CFunc0) objc_message_sendstret02,
    (CFunc0) objc_message_sendstret03,
    (CFunc0) objc_message_sendstret04,
    (CFunc0) objc_message_sendstret05,
    (CFunc0) objc_message_sendstret06,
    (CFunc0) objc_message_sendstret07,
    (CFunc0) objc_message_sendstret08,
    (CFunc0) objc_message_sendstret09,
    (CFunc0) objc_message_sendstret10,
    
    (CFunc0) objc_message_supersendstret00,
    (CFunc0) objc_message_supersendstret01,
    (CFunc0) objc_message_supersendstret02,
    (CFunc0) objc_message_supersendstret03,
    (CFunc0) objc_message_supersendstret04,
    (CFunc0) objc_message_supersendstret05,
    (CFunc0) objc_message_supersendstret06,
    (CFunc0) objc_message_supersendstret07,
    (CFunc0) objc_message_supersendstret08,
    (CFunc0) objc_message_supersendstret09,
    (CFunc0) objc_message_supersendstret10,

    (CFunc0) pf2nsstring_percentdecode_external,
    (CFunc0) pf2nsstring_percentencode_external,
    (CFunc0) pf2nsstring_noncoded_external,
    (CFunc0) nsstring_print_stdout,
    (CFunc0) nsstring_print_stderr,

    (CFunc0) nsbundle_load_bundle
};
#endif

/****************************************************************
** Step 3: Add custom functions to the dictionary.
**     Do not change the name of CompileCustomFunctions!
**     It is called by the pForth kernel.
****************************************************************/

#if (!defined(PF_NO_INIT)) && (!defined(PF_NO_SHELL))
Err CompileCustomFunctions( void )
{
    Err err;
    int i = 0;
/* Compile Forth words that call your custom functions.
** Make sure order of functions matches that in LoadCustomFunctionTable().
** Parameters are: Name in UPPER CASE, Function, Index, Mode, NumParams
*/
    err = CreateGlueToC( "CTEST0", i++, C_RETURNS_VALUE, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "CTEST1", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getsuperclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:ismetaclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getinstancesize]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getinstancevariable]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:getclassvariable]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:addivar]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:copyivarlist]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getivarlayout]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:setivarlayout]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:getweakivarlayout]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:setweakivarlayout]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getproperty]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:copypropertylist]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:addmethod]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:getinstancemethod]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getclassmethod]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:copymethodlist]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:replacemethod]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getmethodimplementation]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:getmethodimplementationstret]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:respondstoselector]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:addprotocol]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:addproperty]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:replaceproperty]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:conformstoprotocol]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:copyprotocollist]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getversion]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:setversion]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:allocateclasspair]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:disposeclasspair]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:registerclasspair]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:createinstance]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:constructinstance]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:destructinstance]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:class:getclasslist]", i++, C_RETURNS_VOID, 0 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:freeclasslist]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:lookupclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getrequiredclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:class:getmetaclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:ivar:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:ivar:gettypeencoding]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:lib:getname]", i++, C_RETURNS_VOID, 0 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:lib:getimagename]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:lib:copyclassnamesforimage]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:sel:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:sel:registername]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:sel:getuid]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:sel:isequal]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:property:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:property:getattributes]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:protocol:getprotocol]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:copyprotocollist]", i++, C_RETURNS_VOID, 0 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:isequal]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:copymethoddescriptionlist]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:conformstoprotocol]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:getproperty]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:copymethoddescription]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:copypropertylist1]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:protocol:copypropertylist2]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:object:copy]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:dispose]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:setinstvar]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:getinstvar]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:getindexedivars]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:getivar]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:getclassname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:getclass]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:object:setclass]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:method:getname]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:getimplementation]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:gettypeencoding]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:copyreturntype]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:copyargumenttype]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:method:getreturntype]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:getnumberofarguments]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:getargumenttype]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:getdescription]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:setimplementation]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:exchangeimplementation]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invoke10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:method:invokestret10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:message:send00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:message:send:super00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:message:send:fpret00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:fpret10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:message:send:stret00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:stret10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[objc:message:send:super:stret00]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret01]", i++, C_RETURNS_VOID, 3 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret02]", i++, C_RETURNS_VOID, 4 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret03]", i++, C_RETURNS_VOID, 5 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret04]", i++, C_RETURNS_VOID, 6 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret05]", i++, C_RETURNS_VOID, 7 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret06]", i++, C_RETURNS_VOID, 8 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret07]", i++, C_RETURNS_VOID, 9 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret08]", i++, C_RETURNS_VOID, 10 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret09]", i++, C_RETURNS_VOID, 11 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[objc:message:send:super:stret10]", i++, C_RETURNS_VOID, 12 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[nsstr:decode:percent]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[nsstr:encode:percent]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[nsstr:noncode:percent]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[nsstr:print:stdout]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;
    err = CreateGlueToC( "[nsstr:print:stderr]", i++, C_RETURNS_VOID, 1 );
    if( err < 0 ) return err;

    err = CreateGlueToC( "[nsbun:load]", i++, C_RETURNS_VOID, 2 );
    if( err < 0 ) return err;
/**
    

**/
    return 0;
}
#else
Err CompileCustomFunctions( void ) { return 0; }
#endif

/****************************************************************
** Step 4: Recompile using compiler option PF_USER_CUSTOM
**         and link with your code.
**         Then rebuild the Forth using "pforth -i system.fth"
**         Test:   10 Ctest0 ( should print message then '11' )
****************************************************************/

#endif  /* PF_USER_CUSTOM */

