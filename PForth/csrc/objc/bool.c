/* bool.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "bool.h"

/* See "Boolean Value" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_bool_getyes(){
	PUSH_DATA_STACK(1);
}

static void objc_bool_getno(){
	PUSH_DATA_STACK(0);
}

static void objc_bool_yesorno(cell_t boolvalue){
	int boolvalue = (int) boolvalue;

	if(boolvalue == 1) {
		PUSH_DATA_STACK(-1);
	} else if (boolvalue == 0){
		PUSH_DATA_STACK(0);
	} else {
		PUSH_DATA_STACK(-100);
	}
}	






