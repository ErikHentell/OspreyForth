/* protocol.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "protocol.h"

/* See "Working with Protocols" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */


static void objc_property_getprotocol(cell_t targetname){
	Protocol* theresult = NULL;
	char* name = (char*) targetname;
	
	theresult = objc_getProtocol(name);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_getprotocollist(){
	unsigned int* outcount = NULL;
	Protocol* theresult = NULL;

	theresult = objc_copyProtocolList(outcount);

	if(*outcount != 0) {
		PUSH_DATA_STACK((cell_t)theresult);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_getname(cell_t targetprotocol){
	Protocol* target = (Protocol*) targetprotocol;
	char* thename = NULL;

	thename = protcol_getName(target);

	if(thename != NULL){
		PUSH_DATA_STACK((cell_t)thename);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_isequal(cell_t leftprot, cell_t rightprot){
	Protocol* left = (Protocol*) leftprot;
	Protocol* right = (Protocol*) rightprot;
	BOOL theresult = NO;

	theresult = protocol_isEqual(left, right);

	if(theresult != NO) {
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_copymethoddescriptionlist(cell_t returnrequired, cell_t returninstance){
	BOOL isrequired = NO;
	BOOL isinstance = NO;
	Protocol* proto = NULL;
	unsigned int* outcount = NULL;
	struct objc_method_description* theresult = NULL;

	if (returnrequired == -1) {
		isrequired = YES;
	} else {
		isrequired = NO;
	}

	if (returninstance == -1) {
		isinstance = YES;
	} else {
		isinstance = NO;
	}
	
	theresult = protocol_copyMethodDescriptionList(proto, isrequired, isinstance, outcount);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_conformstoprotocol(cell_t leftprot, cell_t rightprot){
	Protocol* left = (Protocol*) leftprot;
	Protocol* right = (Protocol*) rightprot;
	BOOL theresult = NO;

	theresult = protocol_conformsToProtocol(left, right);

	if(theresult != NO) {
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_getproperty(cell_t targetprotocol, cell_t targetname, cell_t isrequired, cell_t isinstance){
	Protocol* proto = (Protocol*)targetprotocol;
	char* name = (char*)targetname;
	BOOL returnrequired = NO;
	BOOL returninstance = NO;
	objc_property_t theresult = NULL;

	if (isrequired == -1) {
		returnrequired = YES;
	} else {
		returnrequired = NO;
	}

	if (isinstance == -1) {
		returninstance = YES;
	} else {
		returninstance = NO;
	}

	theresult = protocol_getProperty(proto, name, returnrequired, returninstance);

	if(theresult != NULL){
		PUSH_DATA_STACK(cell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_copymethoddescription(cell_t targetsel, cell_t returnrequired, cell_t returninstance){
	Protocol* proto = NULL;
	SEL thesel = (SEL) targetsel;
	BOOL isrequired = NO;
	BOOL isinstance = NO;
	struct objc_method_description theresult = NULL;

	if (returnrequired == -1) {
		isrequired = YES;
	} else {
		isrequired = NO;
	}

	if (returninstance == -1) {
		isinstance = YES;
	} else {
		isinstance = NO;
	}

	theresult = protocol_getMethodDescription(proto, thesel, isrequired, isinstance);

	if(theresult != {NULL, NULL}){
		PUSH_DATA_STACK((cell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_copypropertylist1(cell_t targetprotocol);
	unsigned int* outcount = NULL;
	Protocol* proto = (Protocol*) targetprotocol;
	objc_property_t* theresult = NULL;

	theresult = protocol_copyPropertyList(proto, outcount);

	if(*outcount != 0) {
		PUSH_DATA_STACK((cell_t)theresult);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_protocol_copyprotocollist2(cell_t targetprotocol){
	unsigned int* outcount = NULL;
	Protocol* proto = (Protocol*) targetprotocol;
	Protocol* theresult = NULL;

	theresult = protocol_copyProtocolList(proto, outcount);

	if(*outcount != 0) {
		PUSH_DATA_STACK((cell_t)theresult);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

