/* ivar.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "ivar.h"

/* See "Working with Instance Variables" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_ivar_getname(cell_t targetivar){
	char* theivarname = NULL;
	Ivar theivar = (Ivar) targetivar;
	
	theivarname = ivar_getName(theivar);

	if(theivarname != NULL) {
		int namelength = strlen(theivarname);
		
		PUSH_DATA_STACK((ucell_t) theivarname);
		PUSH_DATA_STACK(namelength);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_ivar_gettypeencoding(cell_t targetivar){
	char* theencoding = NULL;
	Ivar theivar = (Ivar) targetivar;
	
	theencoding = ivar_getName(theivar);

	if(theencoding != NULL) {
		int namelength = strlen(theencoding);
		
		PUSH_DATA_STACK((ucell_t) theencoding);
		PUSH_DATA_STACK(namelength);
	} else {
		PUSH_DATA_STACK(0);
	}
}






