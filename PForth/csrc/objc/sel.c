/* sel.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "sel.h"

/* See "Working with Selectors" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_sel_getname(cell_t targetsel){
	char* thename = NULL;
	SEL thesel = (SEL) targetselname;
	
	thename = sel_getName(thesel);

	if(thename != NULL){
		int length = strlen(thename);

		PUSH_DATA_STACK((ucell_t)thename);
		PUSH_DATA_STACK(length);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_sel_registername(cell_t targetselname){
	SEL theresult = NULL;
	char* thename = (char*) targetselname;

	theresult = sel_registerName(thename);

	if(theresult != NULL){
		PUSH_DATA_STACK((ucell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_sel_getuid(cell_t targetmethodname){
	SEL theresult = NULL;
	char* thename = (char*) targetmethodname;

	theresult = sel_getUid(thename);

	if(theresult != NULL){
		PUSH_DATA_STACK((ucell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_sel_isequal(cell_t lefthandsel, cell_t righthandsel){
	BOOL theresult = NO;
	SEL lefthand = (SEL) lefthandsel;
	SEL righthand = (SEL) righthandsel;

	theresult = sel_isEqual(lefthand, righthand);

	if(theresult != NO){
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}






