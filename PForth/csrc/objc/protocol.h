/* protocol.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHPROTOCOL_H
#define OBJCPFORTHPROTOCOL_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Protocols" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_protocol_getprotocol(cell_t targetname);
static void objc_protocol_copyprotocollist();
static void objc_protocol_getname(cell_t targetprotocol);
static void objc_protocol_isequal(cell_t leftprot, cell_t rightprot);
static void objc_protocol_copymethoddescriptionlist(cell_t returnrequired, cell_t returninstance);
static void objc_protocol_conformstoprotocol(cell_t leftprot, cell_t rightprot);
static void objc_protocol_getproperty(cell_t targetprotocol, cell_t targetname, cell_t isrequired, cell_t isinstance);
static void objc_protocol_copymethoddescription(cell_t targetsel, cell_t returnrequired, cell_t returninstance);
static void objc_protocol_copypropertylist1(cell_t targetprotocol);
static void objc_protocol_copyprotocollist2(cell_t targetprotocol);

#endif /* OBJCPFORTHPROTOCOL_H */
