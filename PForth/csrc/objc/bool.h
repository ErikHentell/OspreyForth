/* bool.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHBOOL_H
#define OBJCPFORTHBOOL_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Boolean Value" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_bool_getyes();
static void objc_bool_getno();
static void objc_bool_yesorno(cell_t boolvalue);

#endif /* OBJCPFORTHBOOL_H */
