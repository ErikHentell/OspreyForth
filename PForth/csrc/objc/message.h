/* message.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHMESSAGE_H
#define OBJCPFORTHMESSAGE_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Sending Messages" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_message_send00(cell_t targetobj, cell_t targetmethod);
static void objc_message_send01(cell_t targetobj, cell_t targetmethod, cell_t arg1);
static void objc_message_send02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2);
static void objc_message_send03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_message_send04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_message_send05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_message_send06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_message_send07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_message_send08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_message_send09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_message_send10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

static void objc_message_sendstret00(cell_t targetobj, cell_t targetmethod, cell_t structaddr);
static void objc_message_sendstret01(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1);
static void objc_message_sendstret02(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2);
static void objc_message_sendstret03(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_message_sendstret04(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_message_sendstret05(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_message_sendstret06(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_message_sendstret07(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_message_sendstret08(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_message_sendstret09(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_message_sendstret10(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

static void objc_message_sendfpret00(cell_t targetobj, cell_t targetmethod);
static void objc_message_sendfpret01(cell_t targetobj, cell_t targetmethod, cell_t arg1);
static void objc_message_sendfpret02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2);
static void objc_message_sendfpret03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_message_sendfpret04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_message_sendfpret05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_message_sendfpret06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_message_sendfpret07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_message_sendfpret08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_message_sendfpret09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_message_sendfpret10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

static void objc_message_send_super00(cell_t targetobj, cell_t targetmethod);
static void objc_message_send_super01(cell_t targetobj, cell_t targetmethod, cell_t arg1);
static void objc_message_send_super02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2);
static void objc_message_send_super03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_message_send_super04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_message_send_super05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_message_send_super06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_message_send_super07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_message_send_super08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_message_send_super09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_message_send_super10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

static void objc_message_send_superstret00(cell_t targetobj, cell_t targetmethod, cell_t structaddr);
static void objc_message_send_superstret01(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1);
static void objc_message_send_superstret02(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2);
static void objc_message_send_superstret03(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_message_send_superstret04(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_message_send_superstret05(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_message_send_superstret06(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_message_send_superstret07(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_message_send_superstret08(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_message_send_superstret09(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_message_send_superstret10(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

#endif /* OBJCPFORTHMESSAGE_H */
