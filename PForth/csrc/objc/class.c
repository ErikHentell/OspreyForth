/* class.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "class.h"

/* See "Working with Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_getname(cell_t classtarget) {
	int receivedlength = 0;
	char* receivedname = null;
	Class thetarget = (Class) classtarget;

	receivedname = class_getName(thetarget);

	if(receivedname != Nil) {
		receivedlength = strlen(receivedname);

		PUSH_DATA_STACK((cell_t)receivedlength);
		PUSH_DATA_STACK((ucell_t)receivedname);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getsuperclass(cell_t supertarget){
	Class receivedsuper = Nil;
	Class thetarget = (Class)supertarget;
	
	receivedsuper = class_getSuperclass(thetarget);

	if(receivedsuper != Nil) {
		PUSH_DATA_STACK((ucell_t)receivedsuper);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_ismetaclass(cell_t isitmeta){
	Class classtocheck = (Class) isitmeta;
	BOOL checkmark = NO;

	checkmark = class_isMetaClass(classtocheck);

	if(checkmark == YES) {
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getinstancesize(cell_t classtarget){
	Class thetarget = (Class) classtarget;
	size_t thesize = 0;

	thesize = class_getInstanceSize(thetarget);

	PUSH_DATA_STACK((ucell_t) thesize);
}

static void objc_class_getinstancevariable(cell_t classtarget, cell_t thename){
	Ivar theresult = Nil;
	Class thetarget = (Class) classtarget;
	char* namestr = (char*) thename;

	theresult = class_getInstanceVariable(thetarget, namestr);

	if(theresult != Nil) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getclassvariable(cell_t classtarget, cell_t thename){
	Ivar theresult = Nil;
	Class thetarget = (Class) classtarget;
	char* thename = (char*) thename;
	
	theresult = class_getClassVariable(thetarget, thename);

	if(theresult != Nil) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_addivar(cell_t classtarget, cell_t thename, cell_t thesize, cell_t thetypes){
	BOOL theresult = NO;
	Class cls = (Class) classtarget;
	char* name = (char*) thename;
	size_t size = (size_t) thesize;
	uint8_t alignment = 1<<align;	
	char* types = (char*) thetypes;

	theresult = class_addIvar(cls, name, size, alignment, types);

	if(theresult == YES) {
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_copyivarlist(cell_t classtarget){
	unsigned int* thecount = NULL;
	Ivar* theivararray = NULL:
	Class thetarget = (Class) classtarget);

	theivararray = class_copyIvarList(thetarget, thecount);

	if((thecount != NULL) && (theivararray != NULL)) {
		PUSH_DATA_STACK((ucell_t) theivararray);
		PUSH_DATA_STACK(*thecount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getivarlayout(cell_t classtarget){
	uint8_t thelayout = NULL;
	Class thetarget = (Class) classtarget);

	thelayout = class_getIvarLayout(thetarget);

	if(thelayout != NULL) {
		PUSH_DATA_STACK((ucell_t)thelayout);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_setivarlayout(cell_t classtarget, cell_t layouttarget){
	Class thetarget = (Class) classtarget;
	uint8_t* thelayout = (uint8_t*) layouttarget;

	class_setIvarLayout(thetarget, thelayout);
}

static void objc_class_getweakivarlayout(cell_t classtarget){
	uint8_t thelayout = NULL;
	Class thetarget = (Class) classtarget);

	thelayout = class_getWeakIvarLayout(thetarget);

	if(thelayout != NULL) {
		PUSH_DATA_STACK((ucell_t)thelayout);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_setweakivarlayout(cell_t classtarget, cell_t layouttarget){
	Class thetarget = (Class) classtarget;
	uint8_t* thelayout = (uint8_t*) layouttarget;

	class_setWeakIvarLayout(thetarget, thelayout);
}

static void objc_class_getproperty(cell_t classtarget, cell_t thename){
	objc_property_t theresult = Nil;
	Class thetarget = (Class) classtarget;
	char* namestr = (char*) thename;

	theresult = class_getProperty(thetarget, namestr);

	if(theresult != NULL) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_class_copypropertylist(cell_t classtarget){
	unsigned int* thecount = NULL;
	objc_property_t* thepropertyarray = NULL:
	Class thetarget = (Class) classtarget);

	thepropertyarray = class_copyPropertyList(thetarget, thecount);

	if((thecount != NULL) && (thepropertyarray != NULL)) {
		PUSH_DATA_STACK((ucell_t) thepropertyarray);
		PUSH_DATA_STACK(*thecount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_addmethod(cell_t theclass, cell_t thesel, cell_t theimp, cell_t thetypes){
	BOOL theresult = NO;
	Class targetclass = (Class) theclass;
	SEL targetsel = (SEL) thesel;
	IMP targetimp = (IMP) theimp;
	char* targettypes = (char*) thetypes;

	theresult = class_addMethod(targetclass, taretsel, targetimp, targettypes);

	if(theresult != NO) {
		PUSH_DATA_STACK(-1);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getinstancemethod(cell_t classtarget, cell_t thename){
	Method theresult = Nil;
	Class theclass = (Class) classtarget;
	SEL selname = (SEL) thename);

	theresult = class_getInstanceMethod(theclass, selname);

	if(theresult != NULL) {
		PUSH_DATA_STACK((ucell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getclassmethod(cell_t classtarget, cell_t thename){
	Method theresult = Nil;
	Class theclass = (Class) classtarget;
	SEL selname = (SEL) thename);

	theresult = class_getClassMethod(theclass, selname);

	if(theresult != NULL) {
		PUSH_DATA_STACK((ucell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_copymethodlist(cell_t classtarget){
	unsigned int* thecount = NULL;
	Method* thearray = NULL:
	Class thetarget = (Class) classtarget);

	thearray = class_copyMethodList(thetarget, thecount);

	if((thecount != NULL) && (thearray != NULL)) {
		PUSH_DATA_STACK((ucell_t) thearray);
		PUSH_DATA_STACK(*thecount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_replacemethod(cell_t theclass, cell_t thesel, cell_t theimp, cell_t thetypes){
	Class targetclass = (Class) theclass;
	SEL targetsel = (SEL) thesel;
	IMP targetimp = (IMP) theimp;
	char* targettypes = (char*) thetypes;

	class_addMethod(targetclass, taretsel, targetimp, targettypes);
}

static void objc_class_getmethodimplementation(cell_t theclass, cell_t thesel){
	IMP theresult = Nil;
	Class classtarget = (Class) theclass;
	SEL seltarget = (SEL) thesel;

	theresult = class_getMethodImplementation(classtarget, seltarget);

	if(theresult != NULL) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
	
static void objc_class_getmethodimplementationstret(cell_t theclass, cell_t thesel){
	IMP theresult = Nil;
	Class classtarget = (Class) theclass;
	SEL seltarget = (SEL) thesel;

	theresult = class_getMethodImplementation_stret(classtarget, seltarget);

	if(theresult != NULL) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_respondstoselector(cell_t theclass, cell_t thesel){
	BOOL theresult = NO;
	Class targetclass = (Class) theclass;
	SEL targetsel = (SEL) thesel;

	theresult = class_respondsToSelector(targetclass, targetsel);

	if(theresult == YES) {
		PUSH_DATA_STACK(-1);
	}else{
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_addprotocol(cell_t theclass, cell_t theprotocol){
	BOOL theresult = NO;
	Class targetclass = (Class)theclass;
	Protocol* targetprotocol = (Protocol*)theprotocol;

	theresult = class_addProtocol(targetclass, targetprotocol);

	if(theresult == YES) {
		PUSH_DATA_STACK(-1);
	}else{
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_addproperty(cell_t targetclass, cell_t targetname, cell_t attribarray, cell_t attribcount){
	BOOL theresult = NO;
	Class theclass = (Class)targetclass;
	char* thename = (char*)targetname;
	objc_property_attribute_t* theattributes = (objc_property_attribute_t*)attribarray;
	int theattributecount = (int) attribcount; 

	theresult = class_addProperty(theclass, thename, theattributes, theattributecount);

	if(theresult == YES) {
		PUSH_DATA_STACK(-1);
	}else{
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_replaceproperty(cell_t targetclass, cell_t targetname, cell_t attribarray, cell_t attribcount){
	Class theclass = (Class)targetclass;
	char* thename = (char*)targetname;
	objc_property_attribute_t* theattributes = (objc_property_attribute_t*)attribarray;
	int theattributecount = (int) attribcount; 

	class_addProperty(theclass, thename, theattributes, theattributecount);
}

static void objc_class_conformstoprotocol(cell_t theclass, cell_t theprotocol){
	BOOL theresult = NO;
	Class targetclass = (Class)theclass;
	Protocol* targetprotocol = (Protocol*)theprotocol;

	theresult = class_conformsToProtocol(targetclass, targetprotocol);

	if(theresult == YES) {
		PUSH_DATA_STACK(-1);
	}else{
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_copyprotocollist(cell_t classtarget){
	unsigned int* thecount = NULL;
	Protocol* thearray = NULL:
	Class thetarget = (Class) classtarget);

	thearray = class_copyProtocolList(thetarget, thecount);

	if((thecount != NULL) && (thearray != NULL)) {
		PUSH_DATA_STACK((ucell_t) thearray);
		PUSH_DATA_STACK(*thecount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getversion(cell_t classtarget){
	int theresult = 0;
	Class theclass = (Class) classtarget);

	theresult = class_getVersion(theclass);

	PUSH_DATA_STACK(theresult);
}

static void objc_class_setversion(cell_t classtarget, cell_t versiontarget){
	int theversion = (int) versiontarget;
	Class theclass = (Class) classtarget;

	class_setVersion(theclass, theversion);
}

/* See "Adding Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_allocateclasspair(cell_t superclass, cell_t targetname, cell_t extrabytes){
	Class theresult = Nil;
	Class theclass = (Class) superclass;
	char* thename = (char*) targetname;
	size_t thesize = (size_t) extrabytes;

	theresult = objc_allocateClassPair(theclass, thename, thesize);

	if(theresult != Nil) {
		PUSH_DATA_STACK((ucell_t)theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_disposeclasspair(cell_t targetclass){
	Class disposedclass = (Class) targetclass;

	objc_disposeClassPair(disposedclass);
}

static void objc_class_registerclasspair(cell_t targetclass){
	Class registeredclass = (Class) targetclass;

	objc_registerClassPair(registeredclass);
}

/* See "Instantiating Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_createinstance(cell_t targetclass, cell_t extrabytes){
	id theresult = Nil;
	Class theclass = (Class) targetclass;
	size_t thebytes = (size_t) extrabytes;

	theresult = class_createInstance(theclass, thebytes);

	if (theresult != Nil) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_constructinstance(cell_t targetclass, cell_t memorylocation){
	id theresult = Nil;
	Class theclass = (Class) targetclass;
	void* location = (void*) memorylocation;

	theresult = objc_constructInstance(theclass, location);

	if (theresult != Nil) {
		PUSH_DATA_STACK((ucell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_destructinstance(cell_t targetclass){
	Class theclass = (Class) targetclass;

	objc_destructInstance(theclass);
}

/* See "Obtaining Class Definitions" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_getclasslist(){
	int classquantity = 0;
	Class* buffer = NULL;

	classquantity = objc_getClassList(NULL, 0);

	if(classquantity != 0) {
		buffer = malloc(sizeof(Class) * classquantity);
		classquantity = objc_getClassList(buffer, classquantity);
		
		PUSH_DATA_STACK((ucell_t) buffer);
		PUSH_DATA_STACK(classquantity);
	}
}

static void objc_class_freeclasslist(cell_t classbuffer){
	free((Class*) classbuffer);
}

static void objc_class_lookupclass(cell_t targetname){
	Class foundclass = Nil;
	char* thename = (char*) targetname;

	foundclass = objc_lookUpClass(thename);

	if(foundclass != Nil) {
		PUSH_DATA_STACK((ucell_t)foundclass);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getclass(cell_t targetname){
	Class foundclass = Nil;
	char* thename = (char*) targetname;

	foundclass = objc_getClass(thename);

	if(foundclass != Nil) {
		PUSH_DATA_STACK((ucell_t)foundclass);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getrequiredclass(cell_t targetname){
	Class foundclass = Nil;
	char* thename = (char*) targetname;

	foundclass = objc_getRequiredClass(thename);

	if(foundclass != Nil) {
		PUSH_DATA_STACK((ucell_t)foundclass);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_class_getmetaclass(cell_t targetname){
	Class foundclass = Nil;
	char* thename = (char*) targetname;

	foundclass = objc_getMetaClass(thename);

	if(foundclass != Nil) {
		PUSH_DATA_STACK((ucell_t)foundclass);
	} else {
		PUSH_DATA_STACK(0);
	}
}






