/* sel.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHSEL_H
#define OBJCPFORTHSEL_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Selectors" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_sel_getname(cell_t targetsel);
static void objc_sel_registername(cell_t targetselname);
static void objc_sel_getuid(cell_t targetmethodname);
static void objc_sel_isequal(cell_t lefthandsel, cell_t righthandsel);

#endif /* OBJCPFORTHSEL_H */
