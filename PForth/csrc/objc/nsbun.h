/* sel.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHNSBUNDLE_H
#define OBJCPFORTHNSBUNDLE_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See the NSBundle API  in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/foundation/nsbundle?language=objc */

static void nsbundle_load_bundle(cell_t nsstringpath, cell_t nsstringclass);

#endif /* OBJCPFORTHNSBUNDLE_H */
