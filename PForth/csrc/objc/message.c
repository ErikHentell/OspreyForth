/* message.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "message.h"

/* See "Sending Messages" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_message_send00(cell_t targetobj, cell_t targetmethod){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send01(cell_t targetobj, cell_t targetmethod, cell_t arg1){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;
	id thearg10 = (id) arg10;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9, thearg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

/* Messages to the Super Object; targetobj below refer to the Super Class/Super Object */

static void objc_message_send_super00(cell_t targetobj, cell_t targetmethod){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super01(cell_t targetobj, cell_t targetmethod, cell_t arg1){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_send_super10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;
	id thearg10 = (id) arg10;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9, thearg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

/* For the most part, this is unused except on ppc Mac OSX 10.4 and earlier */
/* PPC handles 0 and floating point values differently, but x86 allows them */
/* to be used as necessary to fulfill a job. This makes the function more   */
/* or less the same as objc_message_send functions listed above. */

static void objc_message_sendfpret00(cell_t targetobj, cell_t targetmethod){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret01(cell_t targetobj, cell_t targetmethod, cell_t arg1){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendfpret10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;
	id thearg10 = (id) arg10;

	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9, thearg10);

	if(theresult != 0) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

/* Structure return - Must deliver a struct address along with base object and method pointers */

static void objc_message_sendstret00(cell_t targetobj, cell_t targetmethod, cell_t structaddr){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret01(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret02(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret03(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret04(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4 );

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret05(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret06(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret07(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret08(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret09(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_sendstret10(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;
	id thearg10 = (id) arg10;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9, thearg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

/* Superclass Structure return - Must deliver a struct address along with base object and method pointers */

static void objc_message_supersendstret00(cell_t targetobj, cell_t targetmethod, cell_t structaddr){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret01(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret02(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret03(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret04(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4 );

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret05(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret06(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret07(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret08(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret09(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_message_supersendstret10(cell_t targetobj, cell_t targetmethod, cell_t structaddr, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	SEL thesel = (SEL) targetmethod;
	cell_t theresult = NULL;

	id thearg1 = (id) arg1;
	id thearg2 = (id) arg2;
	id thearg3 = (id) arg3;
	id thearg4 = (id) arg4;
	id thearg5 = (id) arg5;
	id thearg6 = (id) arg6;
	id thearg7 = (id) arg7;
	id thearg8 = (id) arg8;
	id thearg9 = (id) arg9;
	id thearg10 = (id) arg10;

	theresult = (cell_t) objc_msgSend(theobject, thesel, (void *)structaddr, thearg1, thearg2, thearg3, thearg4, thearg5, thearg6, thearg7, thearg8, thearg9, thearg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
