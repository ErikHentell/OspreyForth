/* library.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "library.h"

/* See "Working with Libraries" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_lib_copyimagenames(){
	unsigned int* outcount = NULL;
	char** namebuffer = NULL:

	namebuffer = objc_copyImageNames(outcount);

	if(namebuffer != NULL){
		PUSH_DATA_STACK((ucell_t)namebuffer);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_lib_getimagename(cell_t targetclass){
	char* theresult = NULL;
	Class theclass = (Class) targetclass;

	theresult = class_getImageName(theclass);

	if(theresult != NULL){
		int length = strlen(theresult);

		PUSH_DATA_STACK((ucell_t)theresult);
		PUSH_DATA_STACK(length);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_lib_copyclassnamesforimage(cell_t targetlib){
	unsigned int* outcount = NULL;
	char** namebuffer = NULL:
	char* imagename = (char*)targetlib;

	namebuffer = objc_copyClassNamesForImage(imagename, outcount);

	if(namebuffer != NULL){
		PUSH_DATA_STACK((ucell_t)namebuffer);
		PUSH_DATA_STACK(*outcount);
	} else {
		PUSH_DATA_STACK(0);
	}
}






