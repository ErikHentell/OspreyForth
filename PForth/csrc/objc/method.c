/* method.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "method.h"

/* See "Working with Methods" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_method_getname(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	SEL theresult = NULL;

	theresult = method_getName(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_getimplementation(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	IMP theresult = NULL;

	theresult = method_getImplementation(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_gettypeencoding(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	char* theresult = NULL;

	theresult = method_getTypeEncoding(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
		PUSH_DATA_STACK((cell_t) strlen(theresult));
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_copyreturntype(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	char* theresult = NULL;

	theresult = method_copyReturnType(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
		PUSH_DATA_STACK((cell_t) strlen(theresult));
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_copyargumenttype(cell_t targetmethod, cell_t index){
	Method themeth = (Method) targetmethod;
	unsigned int theindex = (int) index;
	char* theresult = NULL;

	theresult = method_copyArgumentType(themeth, theindex);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
		PUSH_DATA_STACK((cell_t) strlen(theresult));
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_getreturntype(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	char* thedest = NULL;
	size_t thedestlen = NULL;

	method_getReturnType(themeth, thedest, thedestlen);

	if(thedest != NULL) {
		PUSH_DATA_STACK((cell_t) thedest);
		PUSH_DATA_STACK((cell_t) thedestlen);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_getnumberofarguments(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	unsigned int theresult = NULL;

	theresult = method_getNumberofArguments(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_getargumenttype(cell_t targetmethod, cell_t targetidx){
	Method themeth = (Method) targetmethod;
	unsigned int theindex = (int)targetidx;
	char* thedest = NULL;
	size_t thedestlen = NULL;

	method_getArgumentType(themeth, theindex, thedest, thedestlen);

	if(thedest != NULL) {
		PUSH_DATA_STACK((cell_t) thedest);
		PUSH_DATA_STACK((cell_t) thedestlen);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_method_getdescription(cell_t targetmethod){
	Method themeth = (Method) targetmethod;
	struct objc_method_description* theresult = NULL;

	theresult = method_getDescription(themeth);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_setimplementation(cell_t targetmethod, cell_t targetimp){
	Method themeth = (Method) targetmethod;
	IMP theimp = (IMP) targetimp;
	IMP theresult = NULL;
	
	theresult = method_setImplementation(themeth, theimp);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_exchangeimplementations(cell_t target1, cell_t target2){
	Method themeth1 = (Method) target1;
	Method themeth2 = (Method) target2;

	method_exchangeImplementations(target1, target2);
}

static void objc_method_invoke00(cell_t targetobj, cell_t targetmethod){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke01(cell_t targetobj, cell_t targetmethod, cell_t arg1){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invoke10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret00(cell_t targetobj, cell_t targetmethod){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret01(cell_t targetobj, cell_t targetmethod, cell_t arg1){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_method_invokestret10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10){
	id theobject = (id) targetobj;
	IMP themethodimp = (IMP) targetmethod;
	cell_t theresult = NULL;
	
	theresult = (cell_t) method_invoke(theobject, themethodimp, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);

	if(theresult != NULL) {
		PUSH_DATA_STACK(theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

