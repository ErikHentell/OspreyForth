/* instance.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "instance.h"

/* See "Working with Instances" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_object_copy(cell_t targetobject){
	id theobj = (id) targetobject;
	size_t thesize = (size_t) sizeof(theobj);
	id theresult = NULL;
	
	theresult = object_copy(theobj, thesize);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_object_dispose(cell_t targetobject){
	id theobj = (id) targetobject;

	object_dispose(theobj);
}

static void objc_object_setinstvar(cell_t targetobject, cell_t targetname, cell_t value){
	id theobj = (id) targetobject;
	char* thename = (char*) targetname;
	void* theval = (void*) value;
	Ivar theresult = NULL;

	theresult = object_sentInstanceVariable(theobj, thename, theval);

	if(theresult != Nil) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_object_getinstvar(cell_t targetobject, cell_t targetname){
	id theobj = (id)targetobject;
	char* thename = (char*)targetname;
	void** outval = NULL;
	Ivar theresult = NULL;

	theresult = object_getInstanceVariable(theobj, thename, outval);

	if(theresult != Nil) {
		PUSH_DATA_STACK((cell_t) theresult);
		PUSH_DATA_STACK((cell_t) outval);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_object_getindexedivars(cell_t targetobject){
	id theobj = (id) targetobject;
	void* theresult = NULL;

	theresult = object_getIndexedIvars(theobj);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_object_getivar(cell_t targetobject, cell_t targetivar){
	id theobj = (id) targetobject;
	Ivar theivar = (Ivar) targetivar;
	id theresult = NULL;

	theresult = object_getIvar(theobj, theivar);

	if(theresult != nil) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_object_setivar(cell_t targetobject, cell_t targetivar, cell_t targetval){
	id theobj = (id) targetobject;
	Ivar theivar = (Ivar) targetivar;
	id theval = (id) targetval;

	object_setIvar(theobj, theivar, theval);
}
static void objc_object_getclassname(cell_t targetobject){
	id theobj = (id) targetobject;
	char* theresult = NULL;

	theresult = object_getClassName(theobj);

	if(theresult != NULL) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_object_getclass(cell_t targetobject){
	id theobj = (id) targetobject;
	Class theresult = Nil;

	theresult = object_getClass(theobj);

	if(theresult != Nil) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}
static void objc_object_setclass(cell_t targetobject, cell_t targetclass){
	id theobj = (id) targetobject;
	Class theclass = (Class) targetclass;
	Class theresult = NULL;

	theresult = object_setClass(theobj, theclass);

	if(theresult != Nil) {
		PUSH_DATA_STACK((cell_t) theresult);
	} else {
		PUSH_DATA_STACK(0);
	}
}

