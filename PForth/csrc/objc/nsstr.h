/* sel.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHNSSTRING_H
#define OBJCPFORTHNSSTRING_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Selectors" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

/* Internal and therefore not visible to the compiler */
/* id objc_nsstring_create_internal(cell_t sourcestring, cell_t charcount) */
/* id objc_nsstring_percentdecoded_internal(cell_t sourcestring) */
/* id objc_nsstring_percentencoded_internal(cell_t sourcestring) */

static void pf2nsstring_percentdecode_external(cell_t sourcestring, cell_t charcount);
static void pf2nsstring_percentencode_external(cell_t sourcestring, cell_t charcount);
static void pf2nsstring_noncoded_external(cell_t sourcestring, cell_t charcount);
static void nsstring_print_stdout(cell_t printstring);
static void nsstring_print_stderr(cell_t printstring);

#endif /* OBJCPFORTHNSSTRING_H */
