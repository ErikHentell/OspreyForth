/* library.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHLIBRARY_H
#define OBJCPFORTHLIBRARY_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Libraries" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_lib_copyimagenames();
static void objc_lib_getimagename(cell_t targetclass);
static void objc_lib_copyclassnamesforimage(cell_t targetlib);

#endif /* OBJCPFORTHLIBRARY_H */
