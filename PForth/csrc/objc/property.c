/* property.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "property.h"

/* See "Working with Properties" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_property_getname(cell_t targetproperty){
	char* theresult = NULL;
	objc_property_t theproperty = (objc_property_t) targetproperty;
	
	theresult = property_getName(theproperty);

	if(theresult != NULL) {
		int length = strlen(theresult);
	
		PUSH_DATA_STACK((ucell_t)theresult);
		PUSH_DATA_STACK(length);
	} else {
		PUSH_DATA_STACK(0);
	}
}

static void objc_property_getattributes(cell_t targetproperty){
	char* theresult = NULL;
	objc_property_t theproperty = (objc_property_t) targetproperty;
	
	theresult = property_getAttributes(theproperty);

	if(theresult != NULL) {
		int length = strlen(theresult);
	
		PUSH_DATA_STACK((ucell_t)theresult);
		PUSH_DATA_STACK(length);
	} else {
		PUSH_DATA_STACK(0);
	}
}







