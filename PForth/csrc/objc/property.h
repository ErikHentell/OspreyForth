/* property.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHPROPERTY_H
#define OBJCPFORTHPROPERTY_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Properties" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_property_getname(cell_t targetproperty);
static void objc_property_getattributes(cell_t targetproperty);

#endif /* OBJCPFORTHPROPERTY_H */
