/* sel.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "nsstr.h"

/* See the NSString API  in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/foundation/nsstring?language=objc */


/* --- WIP - PRIVATE FUNCTIONS --- */

id cocoa_nsstring_create_internal(cell_t sourcestring, cell_t charcount) {
	id thensstring = objc_getClass("NSString");
	id thensdata = objc_getClass("NSData");
	SEL thedatasel = sel_getUid("dataWithBytes:length:");
	SEL thensstringsel = sel_getUid("initWithData:encoding:");

	id targetdata = objc_msgSend(thensdata, thedatasel, (char *)sourcestring, (NSUInteger) charcount);
	/* 1 is for NSASCIIStringEncoding, the type of encoding to create */
	id targetnsstring = objc_msgSend(thensstring, thensstringsel, targetdata, 1);

	return targetnsstring;
}

id cocoa_nsstring_percentdecoded_internal(cell_t sourcestring) {
	id thesource = (id) sourcestring;
	SEL thesourcesel = sel_getUid("stringByReplacingPercentEscapesUsingEncoding:");

	/* 4 is for NSUTF8StringEncoding, the type of encoding to create */
	id targetnsstring = objc_msgSend(thesource, thesourcesel, 4);

	return targetnsstring;
}

id cocoa_nsstring_percentencoded_internal(cell_t sourcestring) {
	id thesource = (id) sourcestring;
	SEL thesourcesel = sel_getUid("stringByReplacingPercentEscapesUsingEncoding:");

	/* 4 is for NSUTF8StringEncoding, the type of encoding to create */
	id targetnsstring = objc_msgSend(thesource, thesourcesel, 4);

	return targetnsstring;
}

/* --- PUBLIC FUNCTIONS --- */

static void pf2nsstring_percentdecode_external(cell_t sourcestring, cell_t charcount) {
	id targetstring = cocoa_nsstring_create_internal(sourcestring, charcount);

	id newstring = cocoa_nsstring_percentdecoded((cell_t) targetstring);
	
	PUSH_DATA_STACK((ucell_t) newstring);
}

static void pf2nsstring_percentencode_external(cell_t sourcestring, cell_t charcount) {
	id targetstring = cocoa_nsstring_create_internal(sourcestring, charcount);

	id newstring = cocoa_nsstring_percentencoded((cell_t) targetstring);
	
	PUSH_DATA_STACK((ucell_t) newstring);
}

static void pf2nsstring_noncoded_external(cell_t sourcestring, cell_t charcount) {
	id newstring = cocoa_nsstring_create_internal(sourcestring, charcount);

	PUSH_DATA_STACK((ucell_t) newstring);
}

static void nsstring_print_stdout(cell_t printstring) {
	id targetstring = (id) printstring;
	SEL outputsel = sel_getUid("UTF8String");

	id outputstring = objc_msgSend(targetstring, outputsel);

	fprintf(stdout, "%s", outputstring);
}
	
static void nsstring_print_stderr(cell_t printstring) {
	id targetstring = (id) printstring;
	SEL outputsel = sel_getUid("UTF8String");

	id outputstring = objc_msgSend(targetstring, outputsel);

	fprintf(stderr, "%s", outputstring);
}
