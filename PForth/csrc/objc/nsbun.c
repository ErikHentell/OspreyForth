/* nsbun.c
Created by Erik Hentell, 2020
Public Domain
*/

#include "nsbun.h"

/* See the NSBundle API  in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/foundation/nsbundle?language=objc */

/* TODO
1. Implement bundleWithPath to get a new NSBundle object
2. Probably use classNamed: to get the desired controlling class
	a. Must create a named class for a bundle
3. Investigate Runtime methods for getting a principalClass without a name
4. For the time being, the principal class handles all bundle resources without the
   Forth interpreter having to mess with anything
5. Implement code for understanding NSBundle errors as listed in the API page.
6. Do bundles get unloaded?
*/

static void nsbundle_load_bundle(cell_t nsstringpath, cell_t nsstringclass) {
	id thepathstring = (NSString *) nsstringpath;
	id theclassname = (NSString *) nsstringclass;
	id theclass = objc_getClass("NSBundle");
	SEL theselbundle = sel_getUid("bundleWithPath:");
	SEL theselclass = sel_getUid("classNamed:");

	id thebundle = objc_msgSend(theclass, theselbundle, thepathstring);

	if (thebundle != nil) {
		id theloadedclass = objc_msgSend(thebundle, theselclass, theclassname);

		if(theloadedclass != nil) {
			PUSH_DATA_STACK((cell_t) theloadedclass);
			
			return;
		} else {
			PUSH_DATA_STACK(0);

			return;
		}
	} else {
		PUSH_DATA_STACK(0);
	}
}
	
