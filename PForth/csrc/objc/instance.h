/* instance.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHINSTANCE_H
#define OBJCPFORTHINSTANCE_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Instances" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_object_copy(cell_t targetobject);
static void objc_object_dispose(cell_t targetobject);
static void objc_object_setinstvar(cell_t targetobject, cell_t targetname, cell_t value);
static void objc_object_getinstvar(cell_t targetobject, cell_t targetname);
static void objc_object_getindexedivars(cell_t targetobject);

static void objc_object_getivar(cell_t targetobject, cell_t targetivar);
static void objc_object_setivar(cell_t targetobject, cell_t targetivar, cell_t targetval);
static void objc_object_getclassname(cell_t targetobject);
static void objc_object_getclass(cell_t targetobject);
static void objc_object_setclass(cell_t targetobject, cell_t targetclass);

#endif /* OBJCPFORTHINSTANCE_H */
