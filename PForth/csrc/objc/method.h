/* method.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHMETHOD_H
#define OBJCPFORTHMETHOD_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Methods" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_method_getname(cell_t targetmethod);
static void objc_method_getimplementation(cell_t targetmethod);
static void objc_method_gettypeencoding(cell_t targetmethod);
static void objc_method_copyreturntype(cell_t targetmethod);
static void objc_method_copyargumenttype(cell_t targetmethod, cell_t index);

static void objc_method_getreturntype(cell_t targetmethod);
static void objc_method_getnumberofarguments(cell_t targetmethod);
static void objc_method_getargumenttype(cell_t targetmethod, cell_t targetidx);
static void objc_method_getdescription(cell_t targetmethod);
static void objc_method_setimplementation(cell_t targetmethod, cell_t targetimp);
static void objc_method_exchangeimplementations(cell_t target1, cell_t target2);

static void objc_method_invoke00(cell_t targetobj, cell_t targetmethod);
static void objc_method_invoke01(cell_t targetobj, cell_t targetmethod, cell_t arg1);
static void objc_method_invoke02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2);
static void objc_method_invoke03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_method_invoke04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_method_invoke05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_method_invoke06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_method_invoke07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_method_invoke08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_method_invoke09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_method_invoke10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

static void objc_method_invokestret00(cell_t targetobj, cell_t targetmethod);
static void objc_method_invokestret01(cell_t targetobj, cell_t targetmethod, cell_t arg1);
static void objc_method_invokestret02(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2);
static void objc_method_invokestret03(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3);
static void objc_method_invokestret04(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4);
static void objc_method_invokestret05(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5);
static void objc_method_invokestret06(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6);
static void objc_method_invokestret07(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7);
static void objc_method_invokestret08(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8);
static void objc_method_invokestret09(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9);
static void objc_method_invokestret10(cell_t targetobj, cell_t targetmethod, cell_t arg1, cell_t arg2, cell_t arg3, cell_t arg4, cell_t arg5, cell_t arg6, cell_t arg7, cell_t arg8, cell_t arg9, cell_t arg10);

#endif /* OBJCPFORTHMETHOD_H */
