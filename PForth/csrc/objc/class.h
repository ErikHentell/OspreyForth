/* class.h
Created by Erik Hentell, 2020
Public Domain
*/

#ifndef OBJCPFORTHCLASS_H
#define OBJCPFORTHCLASS_H

#include "../pf_all.h"
#include /usr/include/objc/objc.h
#include /usr/include/objc/objc-runtime.h

/* See "Working with Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_getname(cell_t classtarget);
static void objc_class_getsuperclass(cell_t supertarget);
static void objc_class_ismetaclass(cell_t isitmeta);
static void objc_class_getinstancesize(cell_t classtarget);
static void objc_class_getinstancevariable(cell_t classtarget, cell_t thename);

static void objc_class_getclassvariable(cell_t classtarget, cell_t thename);
static void objc_class_addivar(cell_t classtarget, cell_t thename, cell_t thesize, cell_t thetypes);
static void objc_class_copyivarlist(cell_t classtarget);
static void objc_class_getivarlayout(cell_t classtarget);
static void objc_class_setivarlayout(cell_t classtarget, cell_t layouttarget);

static void objc_class_getweakivarlayout(cell_t classtarget);
static void objc_class_setweakivarlayout(cell_t classtarget, cell_t layouttarget);
static void objc_class_getproperty(cell_t classtarget, cell_t thename);
static void objc_class_copypropertylist(cell_t classtarget);
static void objc_class_addmethod(cell_t theclass, cell_t thesel, cell_t theimp, cell_t thetypes);

static void objc_class_getinstancemethod(cell_t classtarget, cell_t thename);
static void objc_class_getclassmethod(cell_t classtarget, cell_t thename);
static void objc_class_copymethodlist(cell_t classtarget);
static void objc_class_replacemethod(cell_t theclass, cell_t thesel, cell_t theimp, cell_t thetypes);
static void objc_class_getmethodimplementation(cell_t theclass, cell_t thesel);

static void objc_class_getmethodimplementationstret(cell_t theclass, cell_t thesel);
static void objc_class_respondstoselector(cell_t theclass, cell_t thesel);
static void objc_class_addprotocol(cell_t theclass, cell_t theprotocol);
static void objc_class_addproperty(cell_t targetclass, cell_t targetname, cell_t attribarray, cell_t attribcount);
static void objc_class_replaceproperty(cell_t targetclass, cell_t targetname, cell_t attribarray, cell_t attribcount);

static void objc_class_conformstoprotocol(cell_t theclass, cell_t theprotocol);
static void objc_class_copyprotocollist(cell_t classtarget);
static void objc_class_getversion(cell_t classtarget);
static void objc_class_setversion(cell_t classtarget, cell_t versiontarget);

/* See "Adding Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_allocateclasspair(cell_t targetclass, cell_t name, cell_t extrabytes);
static void objc_class_disposeclasspair(cell_t targetclass);
static void objc_class_registerclasspair(cell_t targetclass);

/* See "Instantiating Classes" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_createinstance(cell_t targetclass, cell_t extrabytes);
static void objc_class_constructinstance(cell_t targetclass, cell_t memorylocation);
static void objc_class_destructinstance(cell_t targetclass);

/* See "Obtaining Class Definitions" in Apple's Objective-C Runtime Reference */
/* https://developer.apple.com/documentation/objectivec/objective-c_runtime?language=objc */

static void objc_class_getclasslist();
static void objc_class_freeclasslist(cell_t classbuffer);
static void objc_class_lookupclass(cell_t targetname);
static void objc_class_getclass(cell_t targetname);
static void objc_class_getrequiredclass(cell_t targetname);
static void objc_class_getmetaclass(cell_t targetname);

#endif /* OBJCPFORTHCLASS_H */
